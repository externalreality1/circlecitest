#[macro_use]
extern crate yew;
use yew::prelude::*;

struct Model {
    value: String,
}

enum Msg {
    DoIt,
}

impl Component for Model {
    type Message = Msg;
    type Properties = ();
    fn create(_: Self::Properties, _: ComponentLink<Self>) -> Self {
        Self {
            value: String::new(),
        }
    }

    fn update(&mut self, msg: Self::Message) -> ShouldRender {
        match msg {
            Msg::DoIt => self.value = String::from("Thank you for reviewing this for me!"),
        }
        true
    }
}

impl Renderable<Model> for Model {
    fn view(&self) -> Html<Self> {
        html! {
            <div class="container",>
                <h1 class="title",>{ "A CIRCLECI PRODUCTION" }</h1>
                <div id="new_text",>{ &self.value }</div>
                <button id="clk_me", onclick=|_| Msg::DoIt,>{ "click me" }</button>
            </div>
        }
    }
}

fn main() {
    yew::initialize();
    App::<Model>::new().mount_to_body();
    yew::run_loop();
}
