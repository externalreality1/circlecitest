module.exports = {
    'CircleCI Test' : function (client) {
        const link = '.hero-cta';
        client
            .url('http://localhost:8000')
            .waitForElementVisible('body', 10000)
            .assert.visible(link)
            .end();
    }
};
